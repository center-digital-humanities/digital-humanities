<?php get_header(); ?>
			<div class="content main">
				<div class="col full-width" id="main-content" role="main">
					<h1 class="page-title">Visualizations</h1>
					<ol class="visuals">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<li>
							<?php 
							$url = get_field('url', false, false);
							$oembed = _wp_oembed_get_object();
							$provider = $oembed->get_provider($url);
							$oembed_data = $oembed->fetch( $provider, $url );
							$thumbnail = $oembed_data->thumbnail_url;
							?>
							<a href="<?php the_permalink() ?>">
								<img src="<?php echo $thumbnail; ?>" alt="Thumbnail of <?php the_title(); ?> visualization" />
							</a>
							<a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
							<p><?php the_field('short_description'); ?></p>
						</li>

					<?php endwhile; ?>
					</ol>
					
					<?php bones_page_navi(); ?>
					
					<?php else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<section>
							<p>There is nothing available to show here at this time. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

					<?php endif; ?>

				</div>
			</div>

<?php get_footer(); ?>