<?php
/*
 Template Name: List Page
*/
?>
<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title"><?php the_title(); ?></h1>
						<section>
							<?php the_content(); ?>
							
							<?php if( have_rows('list') ): ?>
							
								<ul class="list">
							
								<?php while( have_rows('list') ): the_row(); 
							
									// vars
									$item_title = get_sub_field('title');
									$item_image = get_sub_field('image');
									$item_description = get_sub_field('description');
									$item_button_text = get_sub_field('button_text');
									$item_link = get_sub_field('link');
									if(!empty($item_image)): 
										// vars
										$url = $item_image['url'];
										$title = $item_image['title'];
										// thumbnail
										$size = 'square-thumb';
										$slide = $item_image['sizes'][ $size ];
										$width = $item_image['sizes'][ $size . '-width' ];
										$height = $item_image['sizes'][ $size . '-height' ];
									endif;
								?>
							
									<li>
									<?php if( $item_link && $item_image ) { ?>
										<a href="<?php echo $item_link; ?>">
									<?php } ?>
										<?php if( $item_image ) { ?>
											<img src="<?php echo $item_image['url']; ?>" alt="Logo for <?php echo $title; ?>" class="item-image" />
										<?php } ?>
									<?php if( $item_link && $item_image ) { ?>
										</a>
									<?php } ?>
										<div class="list-details">
										<?php if( $item_link ) { ?>
											<a href="<?php echo $item_link; ?>">
										<?php } ?>
												<h3><?php echo $item_title; ?></h3>
										<?php if( $item_link ) { ?>
											</a>
										<?php } ?>
										<?php if( $item_description ) { ?>
											<?php echo $item_description; ?>
										<?php } ?>
										<?php if( $item_link && $item_button_text ) { ?>
											<a href="<?php echo $item_link; ?>" class="btn">
												<?php echo $item_button_text; ?>
											</a>
										<?php } ?>
										</div>
							
									</li>
							
								<?php endwhile; ?>
							
								</ul>
							
							<?php endif; ?>
						</section>
					</article>

				<?php endwhile; else : ?>

					<article id="post-not-found" <?php post_class( 'cf' ); ?> role="article">
						<h1 class="page-title">Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Consider visiting the <a href="<?php echo home_url(); ?>">homepage</a> or doing a site search.</p>
						</section>
					</article>

				<?php endif; ?>

				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>