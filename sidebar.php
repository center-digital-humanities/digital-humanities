				<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax() || tribe_is_upcoming() && is_tax()) { 
					// Do nothing	
				}
				// For posts
				elseif (is_single() || is_category() || is_search()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( in_category('student-stories') ) { ?>
						<h3>Student Stories</h3>
						<?php $student_query = new WP_Query( array( 'category_name' => 'student-stories', 'showposts' => 5) ); ?>
						<ol class="stories">
							<?php if ($student_query->have_posts()) : while ($student_query->have_posts()) : $student_query->the_post(); ?>
							<li>
								<a href="<?php the_permalink() ?>"><?php if(get_field('student_name')) { ?><?php the_field('student_name'); ?><? } ?></a>
							</li>
						<?php endwhile; ?>
						</ol>
						<a class="btn" href="/category/student-stories/">View All<span class="hidden"> Student Stories</span></a>
						<?php else : endif; ?>
						<?php wp_reset_postdata(); ?>
					
					<?php } else {	
						if ( is_active_sidebar( 'news-sidebar' ) ) : 
							dynamic_sidebar( 'news-sidebar' ); 
						else : endif;
				
						if ( is_active_sidebar( 'events-sidebar' ) ) :
							dynamic_sidebar( 'events-sidebar' );
						else : endif;
					} ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-label="Section Navigation">
							<?php
								// If a Graduate subpage
								if (is_tree(863) || get_field('menu_select') == "graduate") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Graduate', 'bonestheme' ),
										'menu_class' => 'grad-nav',
										'theme_location' => 'grad-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an Undergraduate subpage
								if (is_tree(860) || get_field('menu_select') == "undergraduate") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Undergraduate', 'bonestheme' ),
									   	'menu_class' => 'undergrad-nav',
									   	'theme_location' => 'undergrad-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
					</div>
				</div>
				<?php } ?>